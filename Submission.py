import numpy as np 
import pandas as pd 
from scipy import signal
import matplotlib.pyplot as plt 
import math
from sklearn import preprocessing
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import make_scorer, accuracy_score, confusion_matrix
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt
# Author Jayakrithi Shivakumar 
# Project O2 Care
'''
TThis code gives the output after the nise is removed
'''
# Data Visualisation on Noise Removal 
def Data_Visualisation():
    %matplotlib inline
    # read all datasets files
    for i in range(19):
            df = pd.read_csv('dataset_' + str(i + 1) + '.txt', sep=',', header=None)
            print("dataset_" , i);
            # generating the visualised data for 13 activities 
            for c in range(1, 14):
                count = c;
                print("Accelerometer" , count )
                df_activity = df[df[24] == c].values
                # In this example code, only accelerometer 1 data (column 1 to 3) is used
                for i in range(3):   
                    b, a = signal.butter(4, 0.04, 'lowpass', analog=False)
                    df_activity[:,i] = signal.lfilter(b, a, df_activity[:, i])
                plt.xlabel("Time")
                plt.ylabel("Reading")
                plt.plot(df_activity[500:1500, 0:3])
                plt.show()

            # gyroscope    
                print("gyroscope" , count )
                df_activity = df[df[24] == c].values
            # In this example code, only gyroscope data (column 3 to 6) is used
                for i in range(3):
                    b, a = signal.butter(4, 0.04, 'low', analog=False)
                    for j in range(24):
                        df_activity[:, j] = signal.lfilter(b, a, df_activity[:, j])
                plt.xlabel("Time")
                plt.ylabel("Reading")
                plt.plot(df_activity[500:1500, 3:6])
                plt.show()

    count=count +1

Data_Visualisation();

'''
This code gives the output before the noise removed 
'''
# Data with Noise  

def Remove_Noise():
    %matplotlib inline
    # read all datasets files
    for i in range(19):
            df = pd.read_csv('dataset_' + str(i + 1) + '.txt', sep=',', header=None)
            print("dataset_" , i);
            # generating the visualised data for 13 activities 
            for c in range(1, 14):
                count = c;
                print("Accelerometer" , count )
                df_activity = df[df[24] == c].values
                # In this example code, only accelerometer  data (column 0 to 3) is used
                plt.xlabel("Time")
                plt.ylabel("Reading")
                plt.plot(df_activity[500:1500, 0:3])
                plt.show()


            # gyroscope 
                print("gyroscope" , count )
                df_activity = df[df[24] == c].values
            # In this example code, only gyroscope data (column 3 to 6) is used
                plt.xlabel("Time")
                plt.ylabel("Reading")
                plt.plot(df_activity[500:1500, 3:6])
                plt.show()

    count=count +1

Remove_Noise();



