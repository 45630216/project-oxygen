# Project  Oxygen

This project deals with, visualising sensor data extracted from mainly two devices :
a triaxial accelerometer and a triaxial gyroscope which are at 4 different positions of
the patient’s body. Data is  sampled at 200Hz. The accelerometer records the linear
acceleration and the gyroscope records the rotational acceleration. There are 4 shimmer
sensors which are equipped with 1 accelerometer and 1 gyroscope , which are placed on 
the wrist, chest, hip and ankle. Therefore there are 4 sensors, that are tracking the 
activities of the patients. The activities are recorded as constant values .